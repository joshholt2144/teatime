//
//  main.swift
//  teatime
//
//  Created by Josh Holt on 7/20/16.
//  Copyright © 2016 Josh Holt. All rights reserved.
//

import Foundation
import AppKit
import CoreGraphics

func delay(delay: Double, closure: ()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(),
        closure
    )
}

func flashScreen(times:Int) {
    let inDuration: CGDisplayFadeInterval = 0.5
    let outDuration: CGDisplayFadeInterval = 0.5
    let color = NSColor.greenColor()
    
    var fadeToken: CGDisplayFadeReservationToken = 0
    let colorToUse = color.colorUsingColorSpaceName(NSCalibratedRGBColorSpace)!
    let err = CGAcquireDisplayFadeReservation((inDuration + outDuration)*Float(times), &fadeToken)
    if Int(err.rawValue) != Int(0) {
        NSLog("Error acquiring fade reservation")
        return
    }
    
    for _ in 1...times {
        CGDisplayFade(fadeToken, inDuration,
            0.0 as CGDisplayBlendFraction, 0.2 as CGDisplayBlendFraction,
            Float(colorToUse.redComponent), Float(colorToUse.greenComponent), Float(colorToUse.blueComponent),
            boolean_t(1))
        CGDisplayFade(fadeToken, inDuration,
            0.2 as CGDisplayBlendFraction, 0.0 as CGDisplayBlendFraction,
            Float(colorToUse.redComponent), Float(colorToUse.greenComponent), Float(colorToUse.blueComponent),
            boolean_t(1))
    }
}

if Process.arguments.count < 3 {
    print("teatime requires two arguments space seperated\n")
    print("The First Argument is the amount of time to steep (e.g. 4 for 4 minutes).\n")
    print("The Second Argument is the number of time to flash your screen (e.g. 4).\n")
    print(" Example: teatime 4 10\n")
    print("   This will allow your tea to steep for 4 minutes and then flash your screen 10 times")
    exit(EXIT_FAILURE)
}

let steepTime = Process.arguments[1]
let flashTimes = Process.arguments[2]
let intSteepTime = UInt32(steepTime)!
let intFlashTimes = Int(flashTimes)!

sleep(intSteepTime*60)
flashScreen(intFlashTimes)

